FROM ubuntu:14.04
MAINTAINER Wei
LABEL Description = "This image starts the frontend of WatsonQA"
LABEL Vendor = ""
LABEL Version = "1.0"
ENV LANG C.UTF-8


RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY . /usr/src/app
RUN apt-get update
RUN apt-get install -y git nodejs npm nodejs-legacy curl
RUN npm install -g npm
RUN npm install -g n
RUN n 4.5.0
RUN npm install -g typings
RUN typings install dt~es6-promise dt~es6-collections --global --save
RUN npm -v
RUN node -v
RUN npm install
EXPOSE 3000
CMD ["npm","start"]
